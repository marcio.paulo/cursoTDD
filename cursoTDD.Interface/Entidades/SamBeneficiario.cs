﻿using CursoTDD.DB;

namespace cursoTDD.Interface.Entidades
{
    public class SamBeneficiario: Entidade
    {
        public string CPF { get; set; }
        public string Sexo { get; set; }
    }
}
