﻿namespace cursoTDD.Interface.Entidades
{
    public class Resposta
    {
        public int Codigo { get; set; }
        public string Mensagem { get; set; }
    }
}
