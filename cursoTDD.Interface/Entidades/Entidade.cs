﻿using System;

namespace CursoTDD.DB
{
    public class Entidade
    {
        public Entidade()
        {
            Handle = 0;
        }

        public virtual Int32 Handle { get; set; }

        public virtual T Clonar<T>()
        {
            return (T)this.MemberwiseClone();
        }
    }
}
