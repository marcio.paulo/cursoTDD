﻿using System.Collections.Generic;

namespace cursoTDD.Interface.Prestador
{
    public interface IRnPrestador
    {
        List<string> Validar();
    }
}
