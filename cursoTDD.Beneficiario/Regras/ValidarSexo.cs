﻿using cursoTDD.Interface.Entidades;

namespace cursoTDD.Beneficiario.Regras
{
    public class ValidarSexo : IRegra
    {
        private const string sexoMasculino = "M";
        private const string sexoFeminino = "F";

        public Resposta Validar(SamBeneficiario beneficiario)
        {
            return beneficiario.Sexo != sexoMasculino && beneficiario.Sexo != sexoFeminino ? new Resposta { Codigo = 1, Mensagem = "Erro ao validar" } : new Resposta { Codigo = 0, Mensagem = "Validado Com Sucesso" };
        }
    }
}
