﻿using cursoTDD.Interface.Entidades;

namespace cursoTDD.Beneficiario.Regras
{
    public interface IRegra
    {
        Resposta Validar(SamBeneficiario beneficiario);
    }
}
