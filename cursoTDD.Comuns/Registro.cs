﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using cursoTDD.Infra;

namespace cursoTDD.Comuns
{
    public class Registro
    {
        public static UnityContainer container =
    new UnityContainer();

        public Registro()
        {
            var registro = new cursoTDD.Infra.Registro();

            registro.RegistrarTipo(container);
        }
    }
}
