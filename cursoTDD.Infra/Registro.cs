﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cursoTDD.Beneficiario;
using cursoTDD.Infra.DB;
using cursoTDD.Infra.Entidades;
using cursoTDD.Interface.Beneficiario;
using cursoTDD.Interface.Entidades;
using cursoTDD.Interface.Prestador;
using cursoTDD.Prestador;
using CursoTDD.DB;
using Microsoft.Practices.Unity;

namespace cursoTDD.Infra
{

    public class Registro
    {

        public static UnityContainer _container =
    new UnityContainer();

        public Registro()
        {
            //RegistrarTipo();
        }

        public void RegistrarTipo(IUnityContainer container)
        {
            container
                .RegisterType<IUnityContainer>
                (new InjectionFactory(
                    c => _container))

                .RegisterType<IRnBeneficiario,RnBeneficiario>()
                .RegisterType<IRnPrestador,RnPrestador>()
                .RegisterType<IRepositorio<SamBeneficiario>,
                    RepositorioMysql<SamBeneficiario>>()
                .RegisterType<IRepositorio<SamFamilia>,
                    RepositorioMysql<SamFamilia>>();
        }
    }
}

