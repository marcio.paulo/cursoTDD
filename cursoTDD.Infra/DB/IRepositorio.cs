﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CursoTDD.DB
{
    public interface IRepositorio<T>
    {
        bool EhRepositorioTeste { get; }

        T Incluir(T entity);

        void Incluir(IEnumerable<T> entity);

        T Alterar(T entity);

        void Atualizar(T entity);

        T SaveOrUpdate(T entity);


        void Excluir(T entity);

        void Excluir(int handle);

        void ExcluirVarios(IEnumerable<T> entity);

        void ExcluirVarios(Expression<Func<T, bool>> where);

        // Retorna Todas as Entidades
        IQueryable<T> RetornarTodos();

        // Consulta Todas as Entidades de acordo com condição where
        IQueryable<T> Pesquisar(Expression<Func<T, bool>> where);

        // Consulta pelo handle
        IQueryable<T> Pesquisar(Int32? handle);

        // Permite Expressão LINQ
        IQueryable<T> Pesquisar();

        T FirstOrDefault(Expression<Func<T, bool>> where);

        List<T> ToList(Expression<Func<T, bool>> where);

        // Retorna o nome da tabela mapeada
        string NomeTabela { get; }
    }
}
