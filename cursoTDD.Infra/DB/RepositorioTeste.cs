﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CursoTDD.DB
{
    public class RepositorioTeste<T> : IRepositorio<T> where T : Entidade
    {
        private static readonly List<T> lista = new List<T>();

        public bool EhRepositorioTeste { get; }
        public T Incluir(T entity)
        {
            lock (lista)
            {
                if (entity.Handle <= 0)
                    entity.Handle = OnNovoHandle(entity);
                AtualizaHandles(entity);
                lista.Add(entity.Clonar<T>());

                return entity;
            }
        }

        public void Incluir(IEnumerable<T> entity)
        {
            foreach (T ety in entity)
            {
                Incluir(ety);
            }
        }

        public T Alterar(T entity)
        {
            lock (lista)
            {
                var ent = lista.FirstOrDefault(x => x.Handle == entity.Handle);
                T newEntity;

                if (ent != null)
                {
                    Excluir(entity);
                    newEntity = entity.Clonar<T>();
                    Incluir(newEntity);
                }
                else
                {
                    newEntity = entity;
                }

                return newEntity;
            }
        }

        public void Atualizar(T entity)
        {
            Alterar(entity);
        }

        public T SaveOrUpdate(T entity)
        {
            lock (lista)
            {
                var ent = lista.FirstOrDefault(x => x.Handle == entity.Handle);

                return (ent == null) ? Incluir(entity) : Alterar(entity);
            }
        }

        public void Excluir(T entity)
        {
            throw new NotImplementedException();
        }

        public void Excluir(int handle)
        {
            lock (lista)
            {
                var item = lista.FirstOrDefault(x => x.Handle == handle);
                lista.Remove(item);
            }
        }

        public void ExcluirVarios(IEnumerable<T> entity)
        {
            throw new NotImplementedException();
        }

        public void ExcluirVarios(Expression<Func<T, bool>> @where)
        {
            var entitys = this.Pesquisar(where).ToList();
            foreach (var item in entitys)
                Excluir(item);
        }

        public IQueryable<T> RetornarTodos()
        {
            return Pesquisar();
        }

        public IQueryable<T> Pesquisar(Expression<Func<T, bool>> @where)
        {
            lock (lista)
            {
                return (lista.Select(item => item.Clonar<T>()).ToList())
                    .Select(x => x).AsQueryable().Where(where);
            }
        }

        public IQueryable<T> Pesquisar(int? handle)
        {
            return Pesquisar(x => x.Handle == handle);
        }

        public IQueryable<T> Pesquisar()
        {
            lock (lista)
            {
                return (lista.Select(item => item.Clonar<T>()).ToList())
                    .Select(x => x).AsQueryable();
            }
        }

        public T FirstOrDefault(Expression<Func<T, bool>> @where)
        {
            return Pesquisar(where).FirstOrDefault();
        }

        public List<T> ToList(Expression<Func<T, bool>> @where)
        {
            return Pesquisar(where).ToList();
        }

        public string NomeTabela { get; }

        private int OnNovoHandle(T entidade)
        {
            lock (lista)
            {
                if (lista.Any())
                    return lista.Max(t => t.Handle) + 1;

                return 1;
            }
        }

        private void AtualizaHandles(T entity)
        {
            Type entityType = typeof(T);
            List<PropertyInfo> properties = entityType.GetProperties().ToList();
            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.PropertyType.BaseType == entityType.BaseType)
                {
                    var handleName = "Handle" + propertyInfo.Name;
                    PropertyInfo propertyHandle = entityType.GetProperty(handleName);

                    if (propertyHandle != null && propertyHandle.GetValue(entity, null) == null)
                    {
                        var handle = propertyInfo.GetValue(entity, null);
                        if (handle != null)
                        {
                            propertyHandle.SetValue(entity, (handle as Entidade).Handle, null);
                        }
                    }
                }
            }
        }
    }
}
