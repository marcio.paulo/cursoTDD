﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CursoTDD.DB;

namespace cursoTDD.Infra.DB
{
    public class RepositorioMysql<T>: IRepositorio<T>
    {
        public bool EhRepositorioTeste { get; }
        public T Incluir(T entity)
        {
            throw new NotImplementedException();
        }

        public void Incluir(IEnumerable<T> entity)
        {
            throw new NotImplementedException();
        }

        public T Alterar(T entity)
        {
            throw new NotImplementedException();
        }

        public void Atualizar(T entity)
        {
            throw new NotImplementedException();
        }

        public T SaveOrUpdate(T entity)
        {
            throw new NotImplementedException();
        }

        public void Excluir(T entity)
        {
            throw new NotImplementedException();
        }

        public void Excluir(int handle)
        {
            throw new NotImplementedException();
        }

        public void ExcluirVarios(IEnumerable<T> entity)
        {
            throw new NotImplementedException();
        }

        public void ExcluirVarios(Expression<Func<T, bool>> @where)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> RetornarTodos()
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Pesquisar(Expression<Func<T, bool>> @where)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Pesquisar(int? handle)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Pesquisar()
        {
            throw new NotImplementedException();
        }

        public T FirstOrDefault(Expression<Func<T, bool>> @where)
        {
            throw new NotImplementedException();
        }

        public List<T> ToList(Expression<Func<T, bool>> @where)
        {
            throw new NotImplementedException();
        }

        public string NomeTabela { get; }
    }
}
