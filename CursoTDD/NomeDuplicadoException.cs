﻿using System;

namespace CursoTDD
{
    public class NomeDuplicadoException: Exception
    {
        public NomeDuplicadoException(string mensagem) 
            : base(mensagem)
        {
            
        }
    }
}
