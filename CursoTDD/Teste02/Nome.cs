﻿using System.Collections.Generic;

namespace CursoTDD.Teste02
{
    internal class Nome
    {
        private IList<string> Nomes { get; set; }

        internal IList<string> Incluir(string p)
        {
            if (Nomes == null) Nomes = new List<string>();
            if (!Nomes.Contains(p))
            {
                Nomes.Add(p); 
            }            
            return Nomes;
        }
    }
}