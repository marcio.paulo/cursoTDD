﻿using System.Collections.Generic;
using NUnit.Framework;

namespace CursoTDD.Teste02
{
    [TestFixture]
    public class ListaNomesTeste
    {
        [Test]
        public void Deve_incluir_o_nome_na_lista()
        {
            var nome = new Nome();

            IList<string> retorno = nome.Incluir("Marcio");

            Assert.AreEqual("Marcio", retorno[0],
                "O primeiro nome deve ser Marcio");
        }

        [Test]
        public void Deve_Incluir_dois_nomes_na_lista()
        {
            var nome = new Nome();
            nome.Incluir("Otavio");
            var retorno = nome.Incluir("Maria");
            Assert.IsTrue(retorno.Contains("Otavio"));
            Assert.IsTrue(retorno.Contains("Maria"));
            Assert.AreEqual(2, retorno.Count, "Deve conter 2 nomes");
        }

        [Test]  
        public void Deve_validar_apenas_um_nome_igual()
        {
            var nome = new Nome();
            nome.Incluir("Otavio");
            var retorno = nome.Incluir("Otavio");
            Assert.IsTrue(retorno.Contains("Otavio"));
            
            Assert.AreEqual(1, retorno.Count, "Deve conter 1 nomes");
        }
    }
}