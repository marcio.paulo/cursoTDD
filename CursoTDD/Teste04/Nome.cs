﻿using System.Collections.Generic;

namespace CursoTDD.Teste04
{
    internal class Nome
    {
        private List<string> _meusNomes;

        public Nome()
        {
            _meusNomes = new List<string>();
        }

        public IList<string> Incluir(string p)
        {
            if (!_meusNomes.Contains(p))
                _meusNomes.Add(p);
            return _meusNomes;
        }
    }
}
