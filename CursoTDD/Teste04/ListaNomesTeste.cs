﻿using System.Collections.Generic;
using NUnit.Framework;

namespace CursoTDD.Teste04
{
    [TestFixture]
    public class ListaNomesTeste
    {
        [Test]
        public void Deve_incluir_o_nome_na_lista()
        {
            var nome = new Nome();

            IList<string> retorno = nome.Incluir("Adalberto");

            Assert.AreEqual("Adalberto",retorno[0],"O primeiro nome deve ser Adalberto");
        }

        [Test]
        public void Deve_incluir_dois_nomes_na_lista()
        {
            var nome = new Nome();

            nome.Incluir("Jose");
            var retorno = nome.Incluir("Maria");

            Assert.IsTrue(retorno.Contains("Jose"));
            Assert.IsTrue(retorno.Contains("Maria"));

            Assert.AreEqual(2, retorno.Count, "Deve conter 2 nomes");
        }

        [Test]
        public void Deve_Validar_Apenas_Um_Nome_Igual()
        {
            var nome = new Nome();
            nome.Incluir("Jose");
            var retorno = nome.Incluir("Jose");
            Assert.IsTrue(retorno.Contains("Jose"));
            Assert.AreEqual(1, retorno.Count, "Deve conter 1 nome José");
        }
    }
}
