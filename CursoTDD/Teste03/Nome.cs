﻿using System.Collections.Generic;

namespace CursoTDD
{
    internal class Nome
    {
        private readonly IList<string> _listaNomes;

        public Nome()
        {
            _listaNomes = new List<string>();
        }

        public IList<string> Incluir(string nome)
        {
            if (!_listaNomes.Contains(nome))
            {
                _listaNomes.Add(nome); 
            }
            return _listaNomes;
        }
    }
}