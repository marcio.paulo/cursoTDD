﻿using NUnit.Framework;
using System.Collections.Generic;

namespace CursoTDD.Teste03
{
    [TestFixture]
    public class ListaNomesTeste
    {
        [Test]
        public void Deve_incluir_o_nome_na_lista()
        {
            string nomeInclusao = "Otávio Lindo";
            var nome = new Nome();
            IList<string> retorno = nome.Incluir(nomeInclusao);
            Assert.AreEqual(nomeInclusao, retorno[0], string.Format("O primeiro nome deve ser {0}", nomeInclusao));
        }

        [Test]
        public void Deve_Incluir_dois_nomes_na_lista()
        {
            var nome = new Nome();
            const string jose = "José";
            const string maria = "Maria";

            nome.Incluir(jose);
            var retorno = nome.Incluir(maria);

            Assert.IsTrue(retorno.Contains(jose));
            Assert.IsTrue(retorno.Contains(maria));
           
        }
        [Test]
        public void Deve_validar_apenas_um_nome_igual()
        {
            var nome = new Nome();
            
            nome.Incluir("Jose");
            var retorno = nome.Incluir("Jose");

            Assert.IsTrue(retorno.Contains("Jose"));

            Assert.AreEqual(1, retorno.Count,"Deve conter 1 nome Jose");
            
        }
    }
}