﻿using cursoTDD.Beneficiario.Regras;
using cursoTDD.Interface.Entidades;
using NUnit.Framework;

namespace CursoTDD.Beneficiario
{
    [TestFixture]
    public class RnBeneficarioTest
    {
        public RnBeneficarioTest()
        {
            new cursoTDD.Comuns.Registro();
        }

        [Test]
        public void Deve_validar_Cpf_erro_com_tamanho_maior_que_11()
        {
            var beneficiario = new SamBeneficiario();

            beneficiario.CPF = "123456789000";

            var regra = new ValidarCpf();
            var reposta = regra.Validar(beneficiario);

            Assert.AreEqual(1, reposta.Codigo, reposta.Mensagem);
        }

        [Test]
        public void Deve_validar_Cpf_com_tamanho_igual_onze()
        {
            var beneficiario = new SamBeneficiario();

            beneficiario.CPF = "12345678900";

            var regra = new ValidarCpf();
            var reposta = regra.Validar(beneficiario);

            Assert.AreEqual(0, reposta.Codigo, reposta.Mensagem);
        }

        [Test]
        public void Deve_validar_Cpf_nulo()
        {
            var beneficiario = new SamBeneficiario();

            beneficiario.CPF = null;

            var regra = new ValidarCpfOpcional();
            var reposta = regra.Validar(beneficiario);

            Assert.AreEqual(0, reposta.Codigo, reposta.Mensagem);
        }

        [Test]
        public void Deve_validar_Sexo_Masculino()
        {
            var beneficiario = new SamBeneficiario();

            beneficiario.Sexo = "M";

            var regra = new ValidarSexo();
            var reposta = regra.Validar(beneficiario);

            Assert.AreEqual(0, reposta.Codigo, reposta.Mensagem);
        }

        [Test]
        public void Deve_validar_Sexo_Feminino()
        {
            var beneficiario = new SamBeneficiario();

            beneficiario.Sexo = "F";

            var regra = new ValidarSexo();
            var reposta = regra.Validar(beneficiario);

            Assert.AreEqual(0, reposta.Codigo, reposta.Mensagem);
        }

        [Test]
        public void Deve_validar_Sexo_Erro()
        {
            var beneficiario = new SamBeneficiario();

            beneficiario.Sexo = "X";

            var regra = new ValidarSexo();
            var reposta = regra.Validar(beneficiario);

            Assert.AreEqual(1, reposta.Codigo, reposta.Mensagem);
        }

    }
}
