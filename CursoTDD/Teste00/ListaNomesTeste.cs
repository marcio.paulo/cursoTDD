﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using cursoTDD.Interface.Prestador;
using Microsoft.Practices.Unity;

namespace CursoTDD.Teste00
{
    [TestFixture]
    public class ListaNomesTeste
    {
        [SetUp]
        public void Inicio()
        {
            new cursoTDD.Comuns.Registro();
        }

        [Test]
        public void Deve_incluir_o_nome_na_lista()
        {
            var nome = new Nome();

            List<string> retorno = nome.Incluir("Marcio").ToList();

            Assert.AreEqual("Marcio", retorno[0],
                "O primeiro nome deve ser Marcio");
        }

        [Test]
        public void Deve_incluir_dois_nomes_na_lista()
        {
            var nome = new Nome();

            nome.Incluir("Jose");
            var retorno = nome.Incluir("Maria");

            Assert.IsTrue(retorno.Contains("Jose"));
            Assert.IsTrue(retorno.Contains("Maria"));

            Assert.AreEqual(2, retorno.Count, 
                "Deve conter 2 nomes");
        }

        [Test]
        public void Deve_validar_apenas_um_nome_igual()
        {
            var nome = new Nome();

            nome.Incluir("Jose");
            var retorno = nome.Incluir("Jose");

            Assert.IsTrue(retorno.Contains("Jose"));

            Assert.AreEqual(1, retorno.Count,
                "Deve conter 1 nome Jose");

        }

        [Test]
        public void Teste_referencia()
        {
            var beneficiario = cursoTDD.Comuns
                .Registro.container
                .Resolve<IRnPrestador>();

            var retorno = beneficiario.Validar();

        }
    }

    public class RnPrestadorMock : IRnPrestador
    {
        public List<string> Validar()
        {
            return new List<string>()
            {
                "Marcio",
                "Maria"
            };
        }
    }
}
