﻿using NUnit.Framework;
using System.Collections.Generic;

namespace CursoTDD.Teste01
{
    [TestFixture]
    public class ListaNomesTeste
    {
        [Test]
        public void Deve_incluir_o_nome_na_lista()
        {
            var nome = new Nome();

            List<string> retorno = nome.Incluir("Marcio");

            Assert.AreEqual("Marcio", retorno[0],
                "O primeiro nome deve ser Marcio");
        }

        [Test]
        public void Deve_incluir_dois_nomes_na_lista()
        {
            var nome = new Nome();
            nome.Incluir("Jose");
            var retorno = nome.Incluir("Maria");

            Assert.IsTrue(retorno.Contains("Jose"),"Deve conter o nome Jose");
            Assert.IsTrue(retorno.Contains("Maria"),"Deve conter o nome Maria");

            Assert.AreEqual(2, retorno.Count, "Deve conter 2 nomes");

        }
    }
}
