﻿using System.Collections.Generic;

namespace CursoTDD.Teste01
{
    public class Nome
    {
        private readonly List<string> lista = new List<string>();
        public List<string> Incluir(string nome)
        {
            lista.Add(nome);
            return lista;
        }
    }
}
